import {observable, action} from 'mobx'

class Store {
  @observable todoList = []
  @observable showList = []

  addTodo(d) {
    this.todoList.push(d)
    this.showList = this.todoList
  }

  filterTodoList(f) {
    if (f === 'all') {
      this.showList = this.todoList
      return
    }
    const r = this.todoList.filter(t => {
      return t.completed === f
    })
    this.showList = r
  }

  handleItemClick(id) {
    const r = this.todoList.find(t => {
      return t.id === id
    })
    r.completed = !r.completed

  }

  @action
  change(n, d) {
    this[n] = d
  }

}

const store = new Store()


export default store