import React from 'react'
import {observer} from 'mobx-react'
import store from '../mobx/appStore'

@observer
class TodoList extends React.Component {

  render() {
    const {showList} = store
    return (
      <ul className={'TodoList'}>
        {
          showList.map((v, i) => {
            return (
              <li

                key={v.id}
                onClick={e => store.handleItemClick(v.id)}
                className={!v.completed ? 'active' : 'completed'}
              >
                {i + 1}. {v.content}

              </li>
            )
          })
        }
      </ul>
    )
  }
}

export default TodoList