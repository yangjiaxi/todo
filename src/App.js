import React, {Component} from 'react'
import {observer} from 'mobx-react'
import {Input, Button} from 'antd'
import './index.less'
import store from './mobx/appStore'
import sha256 from 'js-sha256'
import TodoList from './components/TodoList'

const menu = [
  {name: 'all', f: 'all'},
  {name: 'active', f: false,},
  {name: 'completed', f: true,},


]


@observer
class App extends Component {
  changeState(f) {
    store.filterTodoList(f)
  }

  addTodo = e => {
    if (e.key === 'Enter') {
      const content = e.target.value
      const date = new Date()
      const id = sha256(content + date)
      const completed = false
      const newTodo = {
        content, date, id, completed
      }
      store.addTodo(newTodo)
      e.target.value = ''
    }
  }

  render() {
    const style = {
      marginBottom: 10,
    }
    return (
      <div className="App">

        <p style={style}>
          {menu.map(v => {
            return (
              <Button
                key={v.name}
                onClick={e => this.changeState(v.f)}
              >
                {v.name}
              </Button>
            )
          })}
        </p>
        <Input
          onKeyDown={this.addTodo}
        />
        <TodoList/>
      </div>
    )
  }
}

export default App
